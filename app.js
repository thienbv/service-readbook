const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const logger = require("morgan");
const dotenv = require("dotenv");
dotenv.config();
const mongoose = require("mongoose");
const indexRouter = require("./routes/index");
const categorieRouter = require("./routes/category");
const usersRouter = require("./routes/user");
const ratingsRouter = require("./routes/rating");
const viewersRouter = require("./routes/viewer");
const commentRouter = require("./routes/comment");
const booksRouter = require("./routes/book");

//API
const apiBookRouter = require("./routes/apis/api_book");
const apiCategoryRouter = require("./routes/apis/api_category");
const app = express();
//CONNECT DB
mongoose
  .connect(process.env.DB_CONNECTION, {
    useUnifiedTopology: true,
    useNewUrlParser: true
  })
  .then(() => {
    console.log("CONNECT DB ....");
  })
  .catch(err => {
    console.log("ERROR: ", err);
  });
// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);

app.use("/users", usersRouter);
app.use("/categories", categorieRouter);
app.use("/ratings", ratingsRouter);
app.use("/viewers", viewersRouter);
app.use("/comments", commentRouter);
app.use("/books", booksRouter);
//API
app.use("/api/books", apiBookRouter);
app.use("/api/categories", apiCategoryRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
