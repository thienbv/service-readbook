/*******
 * Generated by nemo
 * on 3/6/20
 * version
 ********/
const express = require("express");
const router = express.Router();
const Post = require("../models/Post");
router.get("/", async (req, res) => {
  try {
    const posts = await Post.find();
    res.json(posts);
  } catch (e) {
    res.json({ message: e });
  }
});
//Add post
router.post("/", async (req, res) => {
  let { title, description } = req.body;
  const post = new Post({
    title: title,
    description: description
  });
  try {
    const savePost = await post.save();
    res.json(savePost);
  } catch (err) {
    res.json({ message: "Error save post" });
  }
});

//specific post
router.get("/:postId", async (req, res) => {
  const { postId } = req.params;
  try {
    const post = await Post.findById(postId);
    res.json(post);
  } catch (e) {
    res.json({ message: e });
  }
});

//Delete post
router.delete("/:postId", async (req, res) => {
  let { postId } = req.params;
  try {
    const removePost = await Post.remove({ _id: postId });
    res.json(removePost);
  } catch (e) {
    res.json({ message: e });
  }
});

//Update post
router.patch("/:postId", async (req, res) => {
  try {
    let { postId } = req.params;
    let { title } = req.body;
    const postUpdate = await Post.updateOne(
      { _id: postId },
      {
        $set: {
          title: title
        }
      }
    );
    res.json(postUpdate);
  } catch (e) {
    res.json({ message: e });
  }
});
module.exports = router;
