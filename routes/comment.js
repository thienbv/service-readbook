/*******
 * Generated by nemo
 * on 3/6/20
 * version
 ********/
const express = require("express");
const router = express.Router();
const Comment = require("../models/Comment");
router.get("/", async (req, res) => {
  try {
    const comments = await Comment.find();
    res.json(comments);
  } catch (e) {
    res.json({ message: e });
  }
});
//Add Comment
router.post("/", async (req, res) => {
  let { book_id, user_id, comment } = req.body;
  const commentx = new Comment({
    book_id: book_id,
    user_id: user_id,
    comment: comment
  });
  try {
    const saveComment = await commentx.save();
    res.json(saveComment);
  } catch (err) {
    res.json({ message: "Error save Comment" });
  }
});

//specific Comment
router.get("/:commentId", async (req, res) => {
  const { commentId } = req.params;
  try {
    const comment = await Comment.findById(commentId);
    res.json(Comment);
  } catch (e) {
    res.json({ message: e });
  }
});

//Delete Comment
router.delete("/:commentId", async (req, res) => {
  let { commentId } = req.params;
  try {
    const removeComment = await Comment.remove({ _id: commentId });
    res.json(removeComment);
  } catch (e) {
    res.json({ message: e });
  }
});

//Update Comment
router.patch("/:commentId", async (req, res) => {
  try {
    let { commentId } = req.params;
    let { title } = req.body;
    const commentUpdate = await Comment.updateOne(
      { _id: CommentId },
      {
        $set: {
          title: title
        }
      }
    );
    res.json(commentUpdate);
  } catch (e) {
    res.json({ message: e });
  }
});
module.exports = router;
