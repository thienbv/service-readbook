const express = require("express");
const router = express.Router();
const sessionStorage = require("sessionstorage");
const Book = require("../models/Book");
const Category = require("../models/Category");
const User = require("../models/User");
const utils = require("../utils");
/* GET home page. */
router.get("/", async (req, res, next) => {
  const user = sessionStorage.getItem("user");
  if (!user) {
    res.redirect("/login");
  }
  const totalBook = await Book.count();
  const totalCats = await Category.count();
  res.render("index", {
    title: "Sách Online",
    totalBook: totalBook,
    totalCats: totalCats,
    user: user
  });
});

router.get("/login", async (req, res, next) => {
  res.render("login", {
    title: "Login"
  });
});

router.get("/logout", async (req, res, next) => {
  sessionStorage.removeItem("user");
  res.redirect("/login");
});
router.post("/login", async (req, res, next) => {
  const { email, password } = req.body;
  const user = await User.findOne({ email: email });
  if (user.length == 0) {
    return res
      .status(400)
      .json({ success: false, message: "Wrong Email / Password!" });
  }
  //compare password
  if (utils.checkPassword(password, user.password)) {
    sessionStorage.setItem("user", user);
    return res.status(200).json({ success: true });
  }
  console.log({user})
  return res
    .status(400)
    .json({ success: false, message: "Wrong Email / Password!" });
});
//Validation
const Joi = require("@hapi/joi");
const schema = Joi.object({
  email: Joi.string()
    .min(6)
    .required()
    .email(),
  user_name: Joi.string()
    .min(6)
    .required(),
  password: Joi.string()
    .min(6)
    .required(),
  confirm_password: Joi.string()
    .valid(Joi.ref("password"))
    .required()
});
router.get("/register", async (req, res, next) => {
  res.render("register", {
    title: "Register"
  });
});
router.delete("/removeAll", async (req, res, next) => {
  const remove = await User.remove();
  res.json(remove.deletedCount);
});
router.delete("/users", async (req, res, next) => {
  const users = await User.find();
  res.json(users);
});
router.post("/register", async (req, res, next) => {
  const { email, user_name, password, confirm_password } = req.body;
  const params = {
    email: email,
    user_name: user_name,
    password: password,
    confirm_password: confirm_password
  };
  const validate = schema.validate(params);
  if (validate.error) {
    const errorDetail = validate.error.details[0];
    if (errorDetail.path[0] == "confirm_password") {
      errorDetail.message = "ConfirmPassword must be equal Password";
    }
    return res.status(400).json({
      success: false,
      message: errorDetail.message.replace(/\"/g, "")
    });
  }
  //Check exit
  const checkUser = await User.findOne({ email: email });
  if (checkUser) {
    return res.status(400).json({
      success: false,
      message: email + " is taken"
    });
  }
  //hashPass
  params.password = utils.hashPassword(password);
  const user = new User(params);
  try {
    const userNew = await user.save();
    return res.status(200).json({ success: true, user: userNew });
  } catch (e) {
    return res.status(400).json({ message: e.message() });
  }
});
module.exports = router;
