const bcrypt = require("bcrypt");
const saltRounds = 10;
const dotenv = require("dotenv");
dotenv.config();
const TYPE_BOOK = {
  LATEST: "latest",
  MOST_VIEW: "mostview",
  MOST_RATE: "mostrate"
};
const typesBook = [TYPE_BOOK.LATEST, TYPE_BOOK.MOST_RATE, TYPE_BOOK.MOST_VIEW];
const hashPassword = password => {
  return bcrypt.hashSync(password, saltRounds);
};
const checkPassword = (password, passwordHash) => {
  return bcrypt.compareSync(password, passwordHash);
};
const pagingUtil = (totalBooks, limit, page) => {
  const totalPage = Math.ceil(totalBooks / limit);
  page = page <= totalPage ? page : totalPage;
  let skip = 0;
  if (page && parseInt(page) > 1) {
    skip = limit * (page - 1);
  } else {
    skip = 0;
  }
  return { skip: skip, page: page, totalPage: totalPage };
};
module.exports = {
  hashPassword: hashPassword,
  checkPassword: checkPassword,
  pagingUtil: pagingUtil,
  typesBook: typesBook,
  TYPE_BOOK: TYPE_BOOK
};
